import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  jsonFile: any = null;
  headers: any = null;
  error = '';
  format = `
  [
    {
      "name": "John Doe",
      "gender": "Male",
      "age": 25
    }
  ]`;

  constructor(private _http: HttpClient) { }

  newFile($e: any) {
    this.error = '';
    if ($e.target.files && $e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL($e.target.files[0]);
      reader.onload = (event: ProgressEvent) => {
        const filepath = reader.result;
        if (typeof filepath === 'string') {
          this.readJson(filepath);
        } else {
          this.error = 'Invalid json file';
        }
      };
    }
  }

  readJson($filepath) {
    this._http.get($filepath).subscribe(json => {
      this.jsonFile = json;
      if (json[0]) {
        this.headers = Object.keys(json[0]);
      }
    }, (err) => {
        this.error = 'Invalid json file';
    });
  }

  toKebab($str: string) {
    return $str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  }
}
